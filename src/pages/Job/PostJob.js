import React, { useState } from "react"

import {
    Card,
    CardBody,
    Col,
    Container,
    Form,
    FormGroup,
    Input,
    Label,
    NavItem,
    NavLink,
    Row,
    TabContent,
    TabPane,
} from "reactstrap"
import Select from "react-select";
import classnames from "classnames"
import { Link } from "react-router-dom"
//import react select options from service
import { options } from '../../helpers/ReactSelectOptions';
//Import Breadcrumb
import Breadcrumbs from "../../components/Common/Breadcrumb"

const PostJob = (props) => {

    //meta title
    document.title = "Post Job | BLIV";

    const [activeTab, setactiveTab] = useState(1)
    const [activeTabVartical, setoggleTabVertical] = useState(1)

    const [passedSteps, setPassedSteps] = useState([1])
    const [passedStepsVertical, setPassedStepsVertical] = useState([1])
    const [selectedPayBy, setselectedPayBy] = useState(null);
    const [jobExperience, setJobExperience] = useState("fresher");
    const [benefits, setBenefits] = useState([]);

    function toggleTab(tab) {
        if (activeTab !== tab) {
            var modifiedSteps = [...passedSteps, tab]
            if (tab >= 1 && tab <= 4) {
                setactiveTab(tab)
                setPassedSteps(modifiedSteps)
            }
        }
    }
    const handleSelect = (e) => {
        setselectedPayBy(e.value);
    }

    const onradioChange = (e) => {
        console.log("event==>", e.target.value)
        setJobExperience(e.target.value)
    }

    const addbenefits = (e) => {
        if (e.target.checked) {
            benefits.push(e.target.value)
        } else {
            let index = benefits.findIndex((i) => i === e.target.value)
            benefits.splice(index, 1)
        }
        setBenefits((pre) => [...pre])
    }

    console.log("benefits", benefits)

    function toggleTabVertical(tab) {
        if (activeTabVartical !== tab) {
            var modifiedSteps = [...passedStepsVertical, tab]

            if (tab >= 1 && tab <= 4) {
                setoggleTabVertical(tab)
                setPassedStepsVertical(modifiedSteps)
            }
        }
    }

    return (
        <React.Fragment>
            <div className="page-content">
                <Container fluid={true}>
                    <Breadcrumbs title="Job" breadcrumbItem="Post Job" />
                    <Row>
                        <Col lg="12">
                            <Card>
                                <CardBody>
                                    <div className="wizard clearfix">
                                        <div className="steps clearfix">
                                            <ul>
                                                <NavItem
                                                    className={classnames({ current: activeTab === 1 })}
                                                >
                                                    <NavLink
                                                        className={classnames({ current: activeTab === 1 })}
                                                        onClick={() => {
                                                            setactiveTab(1)
                                                        }}
                                                        disabled={!(passedSteps || []).includes(1)}
                                                    >
                                                        <span className="number">01</span> Job Information
                                                    </NavLink>
                                                </NavItem>
                                                <NavItem
                                                    className={classnames({ current: activeTab === 2 })}
                                                >
                                                    <NavLink
                                                        className={classnames({ active: activeTab === 2 })}
                                                        onClick={() => {
                                                            setactiveTab(2)
                                                        }}
                                                        disabled={!(passedSteps || []).includes(2)}
                                                    >
                                                        <span className="number">02</span> Job Information
                                                    </NavLink>
                                                </NavItem>
                                                <NavItem
                                                    className={classnames({ current: activeTab === 3 })}
                                                >
                                                    <NavLink
                                                        className={classnames({ active: activeTab === 3 })}
                                                        onClick={() => {
                                                            setactiveTab(3)
                                                        }}
                                                        disabled={!(passedSteps || []).includes(3)}
                                                    >
                                                        <span className="number">03</span> Job Information
                                                    </NavLink>
                                                </NavItem>
                                                <NavItem
                                                    className={classnames({ current: activeTab === 4 })}
                                                >
                                                    <NavLink
                                                        className={classnames({ active: activeTab === 4 })}
                                                        onClick={() => {
                                                            setactiveTab(4)
                                                        }}
                                                        disabled={!(passedSteps || []).includes(4)}
                                                    >
                                                        <span className="number">04</span> Confirm Detail
                                                    </NavLink>
                                                </NavItem>
                                            </ul>
                                        </div>
                                        <div className="content clearfix mt-4">
                                            <TabContent activeTab={activeTab}>
                                                <TabPane tabId={1}>
                                                    <Form>
                                                        <Row>
                                                            <Col lg="6">
                                                                <div className="mb-3">
                                                                    <Label for="basicpill-hiringFor-input1">
                                                                        Company you're hiring for
                                                                    </Label>
                                                                    <Input
                                                                        type="text"
                                                                        className="form-control"
                                                                        id="basicpill-hiringFor-input1"
                                                                        name="hiringFor"
                                                                        placeholder="Company you're hiring for"
                                                                    />
                                                                </div>
                                                            </Col>
                                                            <Col lg="6">
                                                                <div className="mb-3">
                                                                    <Label for="basicpill-jobRole-input2">
                                                                        Job role
                                                                    </Label>
                                                                    <Input
                                                                        type="text"
                                                                        className="form-control"
                                                                        id="basicpill-jobRole-input2"
                                                                        name="jobRole"
                                                                        placeholder="Enter your job role"
                                                                    />
                                                                </div>
                                                            </Col>
                                                        </Row>
                                                        <Row>
                                                            <Col lg="6">
                                                                <div className="mb-3">
                                                                    <Label for="basicpill-jobCity-input3">
                                                                        Job city
                                                                    </Label>
                                                                    <Input
                                                                        type="text"
                                                                        className="form-control"
                                                                        id="basicpill-jobCity-input3"
                                                                        name="jobCity"
                                                                        placeholder="Enter your job city"
                                                                    />
                                                                </div>
                                                            </Col>
                                                            <Col lg="6">
                                                                <div className="mb-3">
                                                                    <Label for="basicpill-gender-input4">
                                                                        Gender
                                                                    </Label>
                                                                    <Row>
                                                                        <Col>
                                                                            <input
                                                                                className="form-check-input"
                                                                                type="radio"
                                                                                name="gender"
                                                                                value="male"
                                                                                defaultChecked
                                                                            />
                                                                            &nbsp;&nbsp;
                                                                            <label className="form-check-label" htmlFor="exampleRadios2" >
                                                                                Male
                                                                            </label>
                                                                        </Col>
                                                                        <Col>
                                                                            <input
                                                                                className="form-check-input"
                                                                                type="radio"
                                                                                name="gender"
                                                                                value="female"
                                                                            />
                                                                            &nbsp;&nbsp;
                                                                            <label className="form-check-label" htmlFor="exampleRadios2">
                                                                                Female
                                                                            </label>
                                                                        </Col>
                                                                        <Col>
                                                                            <input
                                                                                className="form-check-input"
                                                                                type="radio"
                                                                                name="gender"
                                                                                value="both"
                                                                            />
                                                                            &nbsp;&nbsp;
                                                                            <label className="form-check-label" htmlFor="exampleRadios2" >
                                                                                Both
                                                                            </label>
                                                                        </Col>
                                                                    </Row>
                                                                </div>
                                                            </Col>
                                                        </Row>
                                                        <Row>
                                                            <Col lg="6">
                                                                <div className="mb-3">
                                                                    <Label for="basicpill-typeOfJob-input5">
                                                                        Type of job
                                                                    </Label>
                                                                    <Row>
                                                                        <Col>
                                                                            <input
                                                                                className="form-check-input"
                                                                                type="radio"
                                                                                name="typeOfJob"
                                                                                value="work from home"
                                                                                defaultChecked
                                                                            />
                                                                            &nbsp;&nbsp;
                                                                            <label className="form-check-label" htmlFor="exampleRadios2">
                                                                                Work from home
                                                                            </label>
                                                                        </Col>
                                                                        <Col>
                                                                            <input
                                                                                className="form-check-input"
                                                                                type="radio"
                                                                                name="typeOfJob"
                                                                                value="work from office"
                                                                            />
                                                                            &nbsp;&nbsp;
                                                                            <label className="form-check-label" htmlFor="exampleRadios2" >
                                                                                Work from office
                                                                            </label>
                                                                        </Col>
                                                                        <Col>
                                                                            <input
                                                                                className="form-check-input"
                                                                                type="radio"
                                                                                name="typeOfJob"
                                                                                value="night shift"
                                                                            />
                                                                            &nbsp;&nbsp;
                                                                            <label className="form-check-label" htmlFor="exampleRadios2" >
                                                                                Night shift
                                                                            </label>
                                                                        </Col>
                                                                    </Row>
                                                                </div>
                                                            </Col>

                                                            <Col lg="6">
                                                                <div className="mb-3">
                                                                    <Label for="basicpill-address-input1">
                                                                        Address
                                                                    </Label>
                                                                    <textarea
                                                                        id="basicpill-address-input1"
                                                                        className="form-control"
                                                                        rows="2"
                                                                        placeholder="Enter Your Address"
                                                                    />
                                                                </div>
                                                            </Col>
                                                        </Row>
                                                        <Row>
                                                            <Col lg="4">
                                                                <div className="mb-3">
                                                                    <Label>Select pay by</Label>
                                                                    <Select
                                                                        defaultValue={(options) => { options.value == selectedPayBy }}
                                                                        onChange={(e) => {
                                                                            handleSelect(e);
                                                                        }}
                                                                        options={options.salary}
                                                                        classNamePrefix="select2-selection"
                                                                    />
                                                                </div>
                                                            </Col>
                                                            {
                                                                selectedPayBy !== null && selectedPayBy == "exact amount" ?
                                                                    <>
                                                                        <Col lg="4">
                                                                            <div className="mb-3">
                                                                                <Label for="basicpill-numbersOfOpening-input3">
                                                                                    Exact salary
                                                                                </Label>
                                                                                <Input
                                                                                    type="text"
                                                                                    className="form-control"
                                                                                    id="basicpill-exactSalary-input3"
                                                                                    name="exactSalary"
                                                                                    placeholder="Enter exact salary"
                                                                                />
                                                                            </div>
                                                                        </Col>
                                                                    </>
                                                                    : null
                                                            }
                                                            {
                                                                selectedPayBy !== null && selectedPayBy == "range" ?
                                                                    <>
                                                                        <Col lg="4">
                                                                            <div className="mb-3">
                                                                                <Label for="basicpill-numbersOfOpening-input3">
                                                                                    Minimum salary
                                                                                </Label>
                                                                                <Input
                                                                                    type="text"
                                                                                    className="form-control"
                                                                                    id="basicpill-minSalary-input3"
                                                                                    name="minSalary"
                                                                                    placeholder="Enter minimum salary"
                                                                                />
                                                                            </div>
                                                                        </Col>
                                                                        <Col lg="4">
                                                                            <div className="mb-3">
                                                                                <Label for="basicpill-numbersOfOpening-input3">
                                                                                    Maximum salary
                                                                                </Label>
                                                                                <Input
                                                                                    type="text"
                                                                                    className="form-control"
                                                                                    id="basicpill-maxSalary-input3"
                                                                                    name="maxSalary"
                                                                                    placeholder="Enter maximum salary"
                                                                                />
                                                                            </div>
                                                                        </Col>
                                                                    </>
                                                                    : null
                                                            }

                                                            <Col lg="4">
                                                                <div className="mb-3">
                                                                    <Label for="basicpill-numbersOfOpening-input3">
                                                                        Numbers of opening
                                                                    </Label>
                                                                    <Input
                                                                        type="text"
                                                                        className="form-control"
                                                                        id="basicpill-numbersOfOpening-input3"
                                                                        name="numbersOfOpening"
                                                                        placeholder="Enter number's of opening"
                                                                    />
                                                                </div>
                                                            </Col>
                                                        </Row>
                                                    </Form>
                                                </TabPane>
                                                <TabPane tabId={2}>
                                                    <div>
                                                        <Form>
                                                            <Row>
                                                                <h5>Education Details</h5>
                                                                <Col lg="6">
                                                                    <div className="mb-3">
                                                                        <Label>Select minimum education</Label>
                                                                        <Select
                                                                            defaultValue={(options) => { options.value == selectedPayBy }}
                                                                            onChange={(e) => {
                                                                                handleSelect(e);
                                                                            }}
                                                                            options={options.education}
                                                                            placeholder="Select minimum education"
                                                                            classNamePrefix="select2-selection"
                                                                        />
                                                                    </div>
                                                                </Col>
                                                                <Col lg="6">
                                                                    <Row className="mb-3">
                                                                        <Label for="basicpill-cstno-input7">
                                                                            Prefered languages
                                                                        </Label>
                                                                        <div className="btn-group" role="group">
                                                                            <input type="checkbox" className="btn-check" id="btncheck4" name="preferedLanguage" autoComplete="off" defaultChecked />
                                                                            <label className="btn btn-outline-primary" htmlFor="btncheck4">Hindi</label>

                                                                            <input type="checkbox" className="btn-check" id="btncheck5" name="preferedLanguage" autoComplete="off" />
                                                                            <label className="btn btn-outline-primary" htmlFor="btncheck5">English</label>

                                                                            <input type="checkbox" className="btn-check" id="btncheck6" name="preferedLanguage" autoComplete="off" />
                                                                            <label className="btn btn-outline-primary" htmlFor="btncheck6">Marathi</label>

                                                                            <input type="checkbox" className="btn-check" id="btncheck7" name="preferedLanguage" autoComplete="off" />
                                                                            <label className="btn btn-outline-primary" htmlFor="btncheck7">kanada</label>
                                                                        </div>
                                                                    </Row>
                                                                </Col>
                                                            </Row>
                                                            <Row>
                                                                <Col lg="6">
                                                                    <div className="mb-3">
                                                                        <Label>What is schedule for job</Label>
                                                                        <Select
                                                                            defaultValue={(options) => { options.value == selectedPayBy }}
                                                                            onChange={(e) => {
                                                                                handleSelect(e);
                                                                            }}
                                                                            options={options.jobShift}
                                                                            placeholder=""
                                                                            classNamePrefix="select2-selection"
                                                                        />
                                                                    </div>
                                                                </Col>
                                                                <Col lg="6">
                                                                    <div className="mb-3">
                                                                        <Label>What is the job type</Label>
                                                                        <Select
                                                                            defaultValue={(options) => { options.value == selectedPayBy }}
                                                                            onChange={(e) => {
                                                                                handleSelect(e);
                                                                            }}
                                                                            options={options.jobTime}
                                                                            placeholder="Select job type"
                                                                            classNamePrefix="select2-selection"
                                                                        />
                                                                    </div>
                                                                </Col>
                                                            </Row>
                                                            <Row>
                                                                <Col lg="6">
                                                                    <div className="mb-3">
                                                                        <Label for="basicpill-jobExp-input5">
                                                                            Job Experience
                                                                        </Label>
                                                                        <Row>
                                                                            <Col>
                                                                                <input
                                                                                    className="form-check-input"
                                                                                    type="radio"
                                                                                    name="jobExp"
                                                                                    value="fresher"
                                                                                    onChange={(e) => { onradioChange(e) }}
                                                                                    defaultChecked
                                                                                />
                                                                                &nbsp;&nbsp;
                                                                                <label className="form-check-label" htmlFor="exampleRadios2">
                                                                                    Fresher
                                                                                </label>
                                                                            </Col>
                                                                            <Col>
                                                                                <input
                                                                                    className="form-check-input"
                                                                                    type="radio"
                                                                                    name="jobExp"
                                                                                    onChange={(e) => { onradioChange(e) }}
                                                                                    value="experienced"
                                                                                />
                                                                                &nbsp;&nbsp;
                                                                                <label className="form-check-label" htmlFor="exampleRadios2" >
                                                                                    Experienced
                                                                                </label>
                                                                            </Col>
                                                                        </Row>
                                                                    </div>
                                                                </Col>
                                                                {

                                                                    jobExperience !== "fresher" ?
                                                                        <>
                                                                            <Col lg="6">
                                                                                <div className="mb-3">
                                                                                    <Label for="basicpill-declaration-input10">
                                                                                        Years of experience
                                                                                    </Label>
                                                                                    <Input
                                                                                        type="number"
                                                                                        className="form-control"
                                                                                        id="yearsOfExp"
                                                                                        placeholder="Enter your years of experience"
                                                                                    />
                                                                                </div>
                                                                            </Col>
                                                                        </>
                                                                        : null
                                                                }
                                                            </Row>
                                                        </Form>
                                                    </div>
                                                </TabPane>
                                                <TabPane tabId={3}>
                                                    <div>
                                                        <Form>
                                                            <Row>
                                                                {/* <Col lg="6">
                                                                    <div className="mb-3">
                                                                        <Label for="basicpill-namecard-input11">
                                                                            Name on Card
                                                                        </Label>
                                                                        <Input
                                                                            type="text"
                                                                            className="form-control"
                                                                            id="basicpill-namecard-input11"
                                                                            placeholder="Enter Your Name on Card"
                                                                        />
                                                                    </div>
                                                                </Col> */}
                                                                <Col lg="12">
                                                                    <div className="mb-3">
                                                                        <Label for="basicpill-address-input1">
                                                                            Address
                                                                        </Label>
                                                                        <textarea
                                                                            id="basicpill-address-input1"
                                                                            className="form-control"
                                                                            rows="4"
                                                                            placeholder="Enter Your Address"
                                                                        />
                                                                    </div>
                                                                </Col>

                                                                {/* <Col lg="6">
                                                                    <div className="mb-3">
                                                                        <Label>Credit Card Type</Label>
                                                                        <select className="form-select">
                                                                            <option defaultValue>
                                                                                Select Card Type
                                                                            </option>
                                                                            <option value="AE">
                                                                                American Express
                                                                            </option>
                                                                            <option value="VI">Visa</option>
                                                                            <option value="MC">MasterCard</option>
                                                                            <option value="DI">Discover</option>
                                                                        </select>
                                                                    </div>
                                                                </Col> */}
                                                            </Row>
                                                            <Row>
                                                                <Col lg="12">
                                                                    <Row className="mb-3">
                                                                        <Label for="basicpill-cstno-input7">
                                                                            Prefered languages
                                                                        </Label>
                                                                        <div className="btn-group" role="group">
                                                                            <input type="checkbox" className="btn-check" id="aadharCard" name="aadharCard" autoComplete="off" defaultChecked />
                                                                            <label className="btn btn-outline-primary" htmlFor="aadharCard">Aadhar Card</label>

                                                                            <input type="checkbox" className="btn-check" id="panCard" name="panCard" autoComplete="off" />
                                                                            <label className="btn btn-outline-primary" htmlFor="panCard">Pan Card</label>

                                                                            <input type="checkbox" className="btn-check" id="drivingLicence" name="drivingLicence" autoComplete="off" />
                                                                            <label className="btn btn-outline-primary" htmlFor="drivingLicence">Driving Licence</label>

                                                                            <input type="checkbox" className="btn-check" id="smartPhone" name="smartPhone" autoComplete="off" />
                                                                            <label className="btn btn-outline-primary" htmlFor="smartPhone">Smart Phone</label>

                                                                            <input type="checkbox" className="btn-check" id="twoWheelers" name="twoWheelers" autoComplete="off" />
                                                                            <label className="btn btn-outline-primary" htmlFor="twoWheelers">Two Wheelers</label>
                                                                        </div>
                                                                    </Row>
                                                                </Col>
                                                                {/* <Col lg="6">
                                                                    <div className="mb-3">
                                                                        <Label for="basicpill-cardno-input12">
                                                                            Credit Card Number
                                                                        </Label>
                                                                        <Input
                                                                            type="text"
                                                                            className="form-control"
                                                                            id="basicpill-cardno-input12"
                                                                            placeholder="Credit Card Number"
                                                                        />
                                                                    </div>
                                                                </Col> */}

                                                                {/* <Col lg="6">
                                                                    <div className="mb-3">
                                                                        <Label for="basicpill-card-verification-input0">
                                                                            Card Verification Number
                                                                        </Label>
                                                                        <Input
                                                                            type="text"
                                                                            className="form-control"
                                                                            id="basicpill-card-verification-input0"
                                                                            placeholder="Credit Verification Number"
                                                                        />
                                                                    </div>
                                                                </Col> */}
                                                            </Row>
                                                            <Row>
                                                                <Col lg="12">
                                                                    <Row className="mb-3">
                                                                        <Label for="basicpill-cstno-input7">
                                                                            Prefered languages
                                                                        </Label>
                                                                        <div className="btn-group" role="group">
                                                                            <input type="checkbox" className="btn-check" id="cellPhoneReimbursement" name="cellPhoneReimbursement" value={"cellPhoneReimbursement"} autoComplete="off" onChange={(e) => addbenefits(e)} />
                                                                            <label className="btn btn-outline-primary text-center" htmlFor="cellPhoneReimbursement">Cell Phone Reimbursement &nbsp;&nbsp;<i className="bx bx-check-double font-size-16 align-middle" ></i></label>

                                                                            <input type="checkbox" className="btn-check" id="cummuterAssistance" name="cummuterAssistance" value={"cummuterAssistance"} onChange={(e) => addbenefits(e)} autoComplete="off" />
                                                                            <label className="btn btn-outline-primary" htmlFor="cummuterAssistance">Cummuter Assistance</label>

                                                                            <input type="checkbox" className="btn-check" id="flexibleSchedule" name="flexibleSchedule" value={"flexibleSchedule"} onChange={(e) => addbenefits(e)} autoComplete="off" />
                                                                            <label className="btn btn-outline-primary" htmlFor="flexibleSchedule">Flexible Schedule</label>

                                                                            <input type="checkbox" className="btn-check" id="foodallowance" name="foodallowance" value={"foodallowance"} autoComplete="off" onChange={(e) => addbenefits(e)} />
                                                                            <label className="btn btn-outline-primary" htmlFor="foodallowance">Food Allowance</label>

                                                                            <input type="checkbox" className="btn-check" id="healthInsurence" name="healthInsurence" autoComplete="off" onChange={(e) => addbenefits(e)} />
                                                                            <label className="btn btn-outline-primary" htmlFor="healthInsurence">Health Insurence</label>

                                                                            <input type="checkbox" className="btn-check" id="internetReimbursement" name="internetReimbursement     " autoComplete="off" onChange={(e) => addbenefits(e)} />
                                                                            <label className="btn btn-outline-primary" htmlFor="internetReimbursement">Health Insurence</label>

                                                                            <input type="checkbox" className="btn-check" id="leaveEncashment" name="leaveEncashment" autoComplete="off" onChange={(e) => addbenefits(e)} />
                                                                            <label className="btn btn-outline-primary" htmlFor="leasveEncashment">leave Encashment</label>
                                                                            
                                                                            <input type="checkbox" className="btn-check" id="lifeInsurence" name="lifeInsurence" autoComplete="off" onChange={(e) => addbenefits(e)} />
                                                                            <label className="btn btn-outline-primary" htmlFor="lifeInsurence">Life Insurence</label>

                                                                            <input type="checkbox" className="btn-check" id="paidSickTime" name="paidSickTime" autoComplete="off" onChange={(e) => addbenefits(e)} />
                                                                            <label className="btn btn-outline-primary" htmlFor="paidSickTime">Paid Sick Time</label>

                                                                            <input type="checkbox" className="btn-check" id="paidTimeOff" name="paidTimeOff" autoComplete="off" onChange={(e) => addbenefits(e)} />
                                                                            <label className="btn btn-outline-primary" htmlFor="paidTimeOff">Paid Time Off</label>

                                                                            <input type="checkbox" className="btn-check" id="providentFund" name="providentFund" autoComplete="off" onChange={(e) => addbenefits(e)} />
                                                                            <label className="btn btn-outline-primary" htmlFor="providentFund">Provident Fund</label>

                                                                            <input type="checkbox" className="btn-check" id="workFromHome" name="workFromHome" autoComplete="off" onChange={(e) => addbenefits(e)} />
                                                                            <label className="btn btn-outline-primary" htmlFor="workFromHome">Work From Home</label>
                                                                        </div>
                                                                    </Row>
                                                                </Col>
                                                            </Row>
                                                        </Form>
                                                    </div>
                                                </TabPane>
                                                <TabPane tabId={4}>
                                                    <div className="row justify-content-center">
                                                        <Col lg="6">
                                                            <div className="text-center">
                                                                <div className="mb-4">
                                                                    <i className="mdi mdi-check-circle-outline text-success display-4" />
                                                                </div>
                                                                <div>
                                                                    <h5>Confirm Detail</h5>
                                                                    <p className="text-muted">
                                                                        If several languages coalesce, the grammar
                                                                        of the resulting
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </Col>
                                                    </div>
                                                </TabPane>
                                            </TabContent>
                                        </div>
                                        <div className="actions clearfix">
                                            <ul>
                                                <li
                                                    className={
                                                        activeTab === 1 ? "previous disabled" : "previous"
                                                    }
                                                >
                                                    <Link
                                                        to="#"
                                                        onClick={() => {
                                                            toggleTab(activeTab - 1)
                                                        }}
                                                    >
                                                        Previous
                                                    </Link>
                                                </li>
                                                <li
                                                    className={activeTab === 4 ? "next disabled" : "next"}
                                                >
                                                    <Link
                                                        to="#"
                                                        onClick={() => {
                                                            toggleTab(activeTab + 1)
                                                        }}
                                                    >
                                                        Next
                                                    </Link>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </div>
        </React.Fragment>
    )
}

export default PostJob
