import React, { useEffect } from "react";
import { Row, Col, CardBody, Card, Alert, Container, Input, Label, Form, FormFeedback } from "reactstrap";

// Formik Validation
import * as Yup from "yup";
import { useFormik } from "formik";


import { Link } from "react-router-dom";

// import images
import profileImg from "../../assets/images/profile-img.png";
import logoImg from "../../assets/images/BLIV_Partner.svg";
import { partnerSignup } from "helpers/validationSchemas";

const Register = props => {

   //meta title
   document.title="Register | BLIV Partner";


  const validation = useFormik({
    // enableReinitialize : use this flag when initial values needs to be changed
    enableReinitialize: true,

    initialValues: {
      hiringFor : 'myOwnCompany',
      companyName : '',
      companyWebsite : '',
      consultancyName : '',
      consultancyWebsite : '',
      numbersOfEmployees : '',
      clients : '',
    },
    validationSchema: partnerSignup,
    onSubmit: (values) => {
    }
  });

  const { values, touched, errors, handleChange, handleBlur, handleSubmit, setFieldValue } = validation


  useEffect(() => {
    dispatch(apiError(""));
  }, []);

  useEffect(() => {
    console.log("values",values)
    console.log("errors",errors)
    console.log("company-register props",props)

  }, [values,errors])


  return (
    <React.Fragment>
      <div className="home-btn d-none d-sm-block">
        <Link to="/" className="text-dark">
          <i className="fas fa-home h2" />
        </Link>
      </div>
      <div className="account-pages my-5 pt-sm-5">
        <Container>
          <Row className="justify-content-center">
            <Col md={8} lg={6} xl={5}>
              <Card className="overflow-hidden">
                <div className="bg-primary bg-soft">
                  <Row>
                    <Col className="col">
                      <div className="text-primary p-5">
                        <h5 className="text-primary text-center m-1">Whom  are you hiring for ?</h5>
                      </div>
                    </Col>
                  </Row>
                </div>
                <CardBody className="pt-0">
                  <div>
                    <Link to="/">
                      <div className="avatar-md profile-user-wid mb-4">
                        <span className="avatar-title rounded-circle bg-light">
                          <img
                            src={logoImg}
                            alt=""
                            //className="rounded-circle"
                            height="44"
                          />
                        </span>
                      </div>
                    </Link>
                  </div>
                  <div className="p-2">
                    <Form
                      className="form-horizontal"
                      onSubmit={(e) => {
                        e.preventDefault();
                        handleSubmit();
                        return false;
                      }}
                    >
                      {user && user ? (
                        <Alert color="success">
                          Register User Successfully
                        </Alert>
                      ) : null}

                      {registrationError && registrationError ? (
                        <Alert color="danger">{registrationError}</Alert>
                      ) : null}
                        <div className="control-group" id="toastTypeGroup">
                          <Row className="controls mb-2 p-3">
                            <Label className="p-0">Choose any option below</Label>
                            <Col className="mb-2 px-auto border mx-2">
                              <input
                                type="radio"
                                id="radio1"
                                name="toastType"
                                className="form-check-input my-2"
                                value="myOwnCompany"
                                defaultChecked
                                onChange={(e)=>{setFieldValue("hiringFor",e.target.value)}}

                              />
                              <Label
                                className="form-check-label mb-2"
                                htmlFor="radio1"
                              >
                                My Company
                                <p className="text-muted form-check-label "><small>I'm an owner/employee of a business or a company/enterprise.</small></p>
                              </Label>
                            </Col>

                            <Col className="mb-2 px-auto border mx-2">
                              <input
                                type="radio"
                                id="radio2"
                                name="toastType"
                                className="form-check-input my-2"
                                value="myClients"
                                onChange={(e)=>{setFieldValue("hiringFor",e.target.value)}}
                              />
                              <Label
                                className="form-check-label  mb-2"
                                htmlFor="radio2"
                              >
                                My Clients
                                <p className="text-muted form-check-label "><small>I'm a consultant working for staffing or manpower consultancy.</small></p>
                              </Label>
                            </Col>
                          </Row>
                        </div>
                      {
                        validation?.values?.hiringFor === "myOwnCompany" ?
                      <>
                      <div className="mb-3">
                        <Label className="form-label">Company Name</Label>
                        <Input
                          id="companyName"
                          name="companyName"
                          className="form-control"
                          placeholder="Enter Your Company Name "
                          type="text"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.companyName || ""}
                          invalid={
                            touched.companyName && errors.companyName ? true : false
                          }
                        />
                        {touched.companyName && errors.companyName ? (
                          <FormFeedback type="invalid">{errors.companyName}</FormFeedback>
                        ) : null}
                      </div>

                      <div className="mb-3">
                        <Label className="form-label">Website Name <small className="text-muted">(optional)</small></Label>
                        <Input
                          name="companyWebsite"
                          type="text"
                          className="form-control"
                          placeholder="Website Name"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.companyWebsite || ""}
                          invalid={
                            touched.companyWebsite && errors.companyWebsite ? true : false
                          }
                        />
                        {touched.companyWebsite && errors.companyWebsite ? (
                          <FormFeedback type="invalid">{errors.companyWebsite}</FormFeedback>
                        ) : null}
                      </div>
                      </>
                      :
                      validation?.values?.hiringFor === "myClients" ?
                      <>
                      <div className="mb-3">
                        <Label className="form-label">Clients you hiring for <small className="text-muted">(comma(,) seperated)</small></Label>
                        <Input
                          name="clients"
                          type="clients"
                          className="form-control"
                          placeholder="e.g. Swiggy,Zomato,...etc"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.clients || ""}
                          invalid={
                            touched.clients && errors.clients ? true : false
                          }
                        />
                        {touched.clients && errors.clients ? (
                          <FormFeedback type="invalid">{errors.clients}</FormFeedback>
                        ) : null}
                      </div>
                      <div className="mb-3">
                        <Label className="form-label">Consultancy Name</Label>
                        <Input
                          id="consultancyName"
                          name="consultancyName"
                          className="form-control"
                          placeholder="Enter Your Consultancy Name "
                          type="text"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.consultancyName || ""}
                          invalid={
                            touched.consultancyName && errors.consultancyName ? true : false
                          }
                        />
                        {touched.consultancyName && errors.consultancyName ? (
                          <FormFeedback type="invalid">{errors.consultancyName}</FormFeedback>
                        ) : null}
                      </div>

                      <div className="mb-3">
                        <Label className="form-label">Consultancy Website Name <small className="text-muted">(optional)</small></Label>
                        <Input
                          id="consultancyWebsite"
                          name="consultancyWebsite"
                          type="text"
                          className="form-control"
                          placeholder="Consultancy Website"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.consultancyWebsite || ""}
                          invalid={
                            touched.consultancyWebsite && errors.consultancyWebsite ? true : false
                          }
                        />
                        {touched.consultancyWebsite && errors.consultancyWebsite ? (
                          <FormFeedback type="invalid">{errors.consultancyWebsite}</FormFeedback>
                        ) : null}
                      </div>
                      </>
                      : null
                      }

                      <div className="mt-4">
                        <button
                          className="btn btn-primary btn-block "
                          type="submit"
                        >
                          Register
                        </button>
                      </div>

                      {/* <div className="mt-4 text-center">
                        <p className="mb-0">
                          By registering you agree to the Skote{" "}
                          <Link to="#" className="text-primary">
                            Terms of Use
                          </Link>
                        </p>
                      </div> */}
                    </Form>
                  </div>
                </CardBody>
              </Card>
              <div className="mt-5 text-center">
                <p>
                  Already have an account ?{" "}
                  <Link to="/login" className="font-weight-medium text-primary">
                    {" "}
                    Login
                  </Link>{" "}
                </p>
                <p>
                  © {new Date().getFullYear()} Skote. Crafted with{" "}
                  <i className="mdi mdi-heart text-danger" /> by Themesbrand
                </p>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </React.Fragment>
  );
};

export default Register;
